import nose
import acp_times
import arrow

# tests example 1 from https://rusa.org/pages/acp-brevet-control-times-calculator
def test_short_brevet_open():
	race_start = arrow.now()

	assert acp_times.open_time(60, 200, race_start.isoformat()) == race_start.shift(minutes=+106).isoformat()
	assert acp_times.open_time(120, 200, race_start.isoformat()) == race_start.shift(minutes=+212).isoformat()
	assert acp_times.open_time(175, 200, race_start.isoformat()) == race_start.shift(minutes=+309).isoformat()
	assert acp_times.open_time(200, 200, race_start.isoformat()) == race_start.shift(minutes=+353).isoformat()

def test_short_brevet_close():
	race_start = arrow.now()

	assert acp_times.close_time(60, 200, race_start.isoformat()) == race_start.shift(minutes=+240).isoformat()
	assert acp_times.close_time(120, 200, race_start.isoformat()) == race_start.shift(minutes=+480).isoformat()
	assert acp_times.close_time(175, 200, race_start.isoformat()) == race_start.shift(minutes=+700).isoformat()
	assert acp_times.close_time(205, 200, race_start.isoformat()) == race_start.shift(minutes=+810).isoformat()

# tests example 2 from https://rusa.org/pages/acp-brevet-control-times-calculator
def test_brevet_with_50km_controls_open():
	race_start = arrow.now()

	assert acp_times.open_time(100, 600, race_start.isoformat()) == race_start.shift(minutes=+176).isoformat()
	assert acp_times.open_time(200, 600, race_start.isoformat()) == race_start.shift(minutes=+353).isoformat()
	assert acp_times.open_time(350, 600, race_start.isoformat()) == race_start.shift(minutes=+634).isoformat()
	assert acp_times.open_time(550, 600, race_start.isoformat()) == race_start.shift(minutes=+1028).isoformat()

def test_brevet_with_50km_controls_close():
	race_start = arrow.now()

	assert acp_times.close_time(550, 600, race_start.isoformat()) == race_start.shift(minutes=+2200).isoformat()
	assert acp_times.close_time(609, 600, race_start.isoformat()) == race_start.shift(minutes=+2400).isoformat()

# tests example 3 - this test is important to check calculation using all distance brackets
def test_1000km_brevet():
	race_start = arrow.now()

	assert acp_times.open_time(890, 1000, race_start.isoformat()) == race_start.shift(minutes=+1749).isoformat()
	assert acp_times.close_time(890, 1000, race_start.isoformat()) == race_start.shift(minutes=+3923).isoformat()

# check start control closure
def test_start_point_closure():
	race_start = arrow.now()

	assert acp_times.close_time(0, 200, race_start.isoformat()) == race_start.shift(minutes=+60).isoformat()
