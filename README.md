# Project 4: Brevet time calculator with Ajax

Author: Patrick Higgins (phiggins@cs.uoregon.edu)

A simple web app that calculates controle open and close times for ACP brevets.
Forked from https://bitbucket.org/UOCIS322/proj4-brevets

## For Developers

### Building

1. Clone this repository.
2. Make credentials.ini by copying the skeleton into brevets/ ```$cp credentials-skel.ini brevets/credentials.ini```, and configure the fields inside.
3. Build and run using the included Dockerfile.

### Brevet Specifications
ACP brevet time calculation is somewhat analogous to tax brackets. Open and close times are calculated using minimum and maximum speeds for designated distance intervals - (0-200km, 200-400km, 400-600km, 600-1000km). For controls that fall within 200km of the start, this is a one-step calculation. However, for controls in higher brackets, the allowable speed for each interval must be considered. For example, the open time for a 450km checkpoint on a 600km brevet requires use of the 0-200km, 200-400km, and 400-600km brackets, and is calculated in multiple steps: (200km / 34km/h) + (200km / 32 km/h) + (50km / 30km/h). Before modifying the implementation of acp_times, carefully read the For Race Organizers section of this README and the linked documentation, taking careful note of special cases and exceptions.
### Testing

The acp_times module includes a nosetests test suite. Run tests with ```cd brevets && nosetests```

## For Race Organizers

Open and close times for controls are calculated per official RUSA rules: https://rusa.org/pages/orgreg

The open and close times for a given control point are based on the maximum and minimum speed guidelines given by the RUSA calculator: https://rusa.org/pages/acp-brevet-control-times-calculator.

There are several important notes in these rules:

*	No controle can close after the maximum allowed time for the brevet has passed: this means that any control beyond the nominal distance of the brevet closes at the brevet's maximum allowed time.

*	The starting control closes one hour after the race begins.

*	As a result of this, it is possible to place a control that closes before the start does - a control placed within 15km of the start will have this result.
 Take care when planning races to avoid tight or impossible legs caused by early controls.
